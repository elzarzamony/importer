<?php

// autoload_classmap.php @generated by Composer

$vendorDir = dirname(dirname(__FILE__));
$baseDir = dirname($vendorDir);

return array(
    'CsvType' => $baseDir . '/classes/CsvType.php',
    'Database' => $baseDir . '/classes/Database.php',
    'Importer' => $baseDir . '/classes/Importer.php',
    'JsonType' => $baseDir . '/classes/JsonType.php',
    'TypesAdopter' => $baseDir . '/classes/TypesAdopter.php',
    'XmlType' => $baseDir . '/classes/XmlType.php',
);
