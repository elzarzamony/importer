<?php

class Importer
{

    public $data;
    public $file;
    public $type;

    /**
     * Undocumented function
     *
     * @param string $data
     * @param bool $file
     * @param string $type
     */
    public function __construct($data, $file, $type=''){
        $this->data = $data;
        $this->file = $file;
        $this->type = $type;        
    }
    
    /**
     * getProducts is a main function that use for implements data from target class, check if returned data is valid  and return it if its valid
     *
     * @return array
     */
    public function getProducts(){
        $obj = $this->getType($this->type);
        $obj->setData($this->data, $this->file);
        if($obj->isType()){
            return $obj->ConvertToArray();
        }
        return [];
    }
    
    /**
     * insert data into database
     * 
     * @param string $query
     * @return bool
     */
    public function insert($query){
        $db = Database::getInstance();
        return $db->query($query);
    }

    public function getType($type=''){
        switch($type){
            case 'csv':
                return new CsvType();
                break;
            case 'xml':
                return new XmlType();
                break;
            default:
                return new JsonType();
        }
    }

}
