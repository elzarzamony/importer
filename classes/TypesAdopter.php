<?php

abstract class TypesAdopter
{
    public $data;
    public $isFile;
    abstract public function setData($data, $file=0);
    abstract public function isType();
    abstract public function ConvertToArray();
    abstract public function getContent($file);

}
