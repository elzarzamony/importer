<?php


class CsvType extends TypesAdopter
{
    /**
     * setData is using for saving income data and check if that data in a file or not
     *
     * @param string $data 
     * @param integer $file
     * @return string
     */
    public function setData($data, $file=0)
    {
        $this->data = ((int)$file) ? $this->getContent($data) : $data;
    }

    /**
     * isType can check if the inserted data is the type that we want or not
     *
     * @return boolean
     */
    public function isType()
    {
        return is_array($this->data);
    }
    
    /**
     * getContent is a simple function to retrieve tha data from the file
     *
     * @param string $file
     * @return string
     */
    public function getContent($file){
        if(file_exists($file)){
            $file = fopen($file,"r");
            $data = [];
            while(! feof($file))
            {
                $data[] = fgetcsv($file);
            }
            fclose($file);
            return $data;
        }
        return null;
    }

    /**
     * ConvertToArray using for return array
     *
     * @return array
     */
    public function ConvertToArray()
    {
        return $this->data;
    }
}
