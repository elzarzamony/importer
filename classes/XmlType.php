<?php


class XmlType extends TypesAdopter
{
    /**
     * setData is using for saving income data and check if that data in a file or not
     *
     * @param string $data 
     * @param integer $file
     * @return string
     */
    public function setData($data, $file=0)
    {
        $this->data = ((int)$file) ? $this->getContent($data) : $data;
    }

    /**
     * isType can check if the inserted data is the type that we want or not
     *
     * @return boolean
     */
    public function isType()
    {
        libxml_use_internal_errors(true);

        $doc = new DOMDocument('1.0', 'utf-8');
        $doc->loadXML($this->data);

        $errors = libxml_get_errors();
        libxml_clear_errors();

        return empty($errors);
    }
    
    /**
     * getContent is a simple function to retrieve tha data from the file
     *
     * @param string $file
     * @return string
     */
    public function getContent($file){
        if(file_exists($file))
            return file_get_contents($file);
        return null;
    }

    /**
     * ConvertToArray using for return array
     *
     * @return array
     */
    public function ConvertToArray()
    {
        return (array)simplexml_load_string($this->data, 'SimpleXMLElement', LIBXML_NOCDATA);
    }
}
