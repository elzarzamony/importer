<?php

class Database
{
    private $db;
    static $instance;

    private function __construct()
    {
        try {
            $this->db = new PDO('mysql:host=localhost;dbname=blog', 'root', '');
            $this->db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
        } catch (PDOException $e) {
            echo 'Connection failed: ' . $e->getMessage();
        }
    }

    private function __clone()
    {}

    public static function getInstance()
    {
        if (!(self::$instance instanceof self)) {
            self::$instance = new self();
        }
        return self::$instance;
    }

    public function query($sql)
    {
        try {
            return $this->db->query($sql);
        } catch (PDOException $e) {
            echo 'Error: ' . $e->getMessage();
        }
    }

}
