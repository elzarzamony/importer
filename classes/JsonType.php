<?php


class JsonType extends TypesAdopter
{
   /**
    * setData is using for saving income data and check if that data in a file or not
    *
    * @param string $data 
    * @param integer $file
    * @return string
    */
    public function setData($data, $file=0)
    {
        $this->data = ((int)$file) ? $this->getContent($data) : $data;
    }

    /**
     * isType can check if the inserted data is the type that we want or not
     *
     * @return boolean
     */
    public function isType()
    {
        json_decode($this->data);
        return (json_last_error() == JSON_ERROR_NONE);
    }

    /**
     * getContent is a simple function to retrieve tha data from the file
     *
     * @param string $file
     * @return string
     */
    public function getContent($file){
        if(file_exists($file))
            return file_get_contents($file);
        return null;
    }

    /**
     * ConvertToArray using for convert data from json to array
     *
     * @return array
     */
    public function ConvertToArray()
    {
        return json_decode($this->data, true);
    }
}
